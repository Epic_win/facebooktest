﻿using System;
using UnityEngine;
using System.Collections;

public class View : MonoBehaviour
{
    public event Action<View> Shown = delegate {  };

    public virtual void Show()
    {
        gameObject.SetActive(true);
        Shown(this);
    }

    public virtual void Hide()
    {
        gameObject.SetActive(false);
    }
}
