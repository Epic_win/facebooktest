﻿using System;
using UnityEngine;
using System.Collections;

public class SocialView : View
{
    [SerializeField]
    private CustomButton _loginButton;
    [SerializeField]
    private CustomButton _shareButton;
    [SerializeField]
    private CustomButton _inviteButton;

    public event Action LogInPressed = delegate { };
    public event Action LogOutPressed = delegate { };
    public event Action SharePressed = delegate { };
    public event Action InvitePressed = delegate { };

    private bool _loggedIn;

    void Awake()
    {
        SetRefeerences();
    }

    private void SetRefeerences()
    {
        _loginButton.Pressed += OnLoginButtonPressed;
        _shareButton.Pressed += OnShareButtonPressed;
        _inviteButton.Pressed += OnInviteButtonPressed;
    }

    private void OnInviteButtonPressed()
    {
        InvitePressed();
    }

    private void OnShareButtonPressed()
    {
        SharePressed();
    }

    private void OnLoginButtonPressed()
    {
        if (_loggedIn)
            LogOutPressed();
        else
            LogInPressed();
    }

    public void UpdateSocial(bool loggedIn)
    {
        _loggedIn = loggedIn;
        _loginButton.Text = loggedIn ? "LOG OUT" : "LOG IN";
        _shareButton.IsActive = loggedIn;
        _inviteButton.IsActive = loggedIn;
    }
}
