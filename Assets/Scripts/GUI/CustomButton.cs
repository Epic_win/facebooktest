﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public class CustomButton : MonoBehaviour
{
    private Button _button;
    private Text _text;

    public string Text { set { _text.text = value; } }

    public bool IsActive
    {
        get { return _button.interactable; }
        set { _button.interactable = value; }
    }

    public event Action Pressed = delegate {  };


    private void Awake()
    {
        _button = GetComponent<Button>();
        _text = GetComponentInChildren<Text>();
        _button.onClick.AddListener(OnButtonPressed);
    }

    private void OnButtonPressed()
    {
        Pressed();
    }
}
