﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MessageView : View
{
    [SerializeField] private Text _textMessage;
    [SerializeField] private Button _okButton;

    public event Action OkPressed = delegate {  };

    void Awake()
    {
        _okButton.onClick.AddListener(OnOkPressed);
    }

    public void Show(string text)
    {
        if (text == null) return;

        base.Show();
        _textMessage.text = text;
    }

    private void OnOkPressed()
    {
        OkPressed();
    }
}
