﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GuiManager : MonoBehaviour
{
    [SerializeField] private View _defaultView;
    [SerializeField] private View[] _views;

    private View _cuurentView;

    void Awake()
    {
        for (int i = 0; i < _views.Length; i++)
        {
            _views[i].Shown += OnViewShown;

            if (_defaultView == _views[i])
                _views[i].Show();
            else
                _views[i].Hide();
        }
    }

    private void OnViewShown(View view)
    {
        if (_cuurentView != null)
            _cuurentView.Hide();

        _cuurentView = view;
    }

    public T GetView<T>() where T : View, new()
    {
        for (int i = 0; i < _views.Length; i++)
        {
            var view = _views[i].GetComponent<T>();
            if (view != null)
                return view;
        }

        Debug.LogError("There is no view of type " + typeof(T).Name);
        return null;
    }
}
