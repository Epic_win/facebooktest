﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Facebook.Unity;
using UnityEngine.UI;
using Logger = UnityEngine.Logger;

public class FacebookController : MonoBehaviour
{
    //private GuiManager _guiManager;

    private readonly List<string> _permissions = new List<string> { "public_profile", "email", "user_friends" };

    private readonly string _url = "https://test.com/";
    private readonly string _appLinkUrl = "https://fb.me/1908672252698005";

    public event Action<bool> LoggedIn = delegate {  }; 
    public event Action<bool> Shared = delegate {  }; 
    public event Action<bool> Invited = delegate {  }; 

    public void Initialize()
    {
        if (!FB.IsInitialized)
            FB.Init(InitCallback);
        else
            FB.ActivateApp();

        LoggedIn(FB.IsLoggedIn);
    }

    private void InitCallback()
    {
        if (FB.IsInitialized)
            FB.ActivateApp();
        else
            Debug.Log("Failed to Initialize the Facebook SDK");
    }

    public void InviteFriends()
    {
        FB.Mobile.AppInvite(new Uri(_appLinkUrl), callback:InviteCallback);
    }

    private void InviteCallback(IAppInviteResult result)
    {
        Invited(!result.Cancelled && String.IsNullOrEmpty(result.Error));
    }

    public void Share()
    {
        FB.ShareLink(new Uri(_appLinkUrl), callback: ShareCallback);
    }

    private void ShareCallback(IShareResult result)
    {
        Shared(!result.Cancelled && String.IsNullOrEmpty(result.Error));
    }

    public void Login()
    {
        FB.LogInWithReadPermissions(_permissions, LoginCallback);
    }

    private void LoginCallback(ILoginResult result)
    {
        LoggedIn(FB.IsLoggedIn);
    }

    public void Logout()
    {
        FB.LogOut();
        LoggedIn(false);
    }
}
