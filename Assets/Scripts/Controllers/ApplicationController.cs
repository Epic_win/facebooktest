﻿using System;
using UnityEngine;
using System.Collections;

public class ApplicationController : MonoBehaviour
{
    [SerializeField] private GuiManager _guiManager;

    private FacebookController _facebookController = new FacebookController();

    private MessageView _messageView;
    private SocialView _socialView;

    private void Start()
    {
        InitializeGui();
        InitialzeFacebook();
    }

    private void InitialzeFacebook()
    {
        _facebookController.LoggedIn += OnLoggedIn;
        _facebookController.Shared += OnShared;
        _facebookController.Invited += OnInvited;
        _facebookController.Initialize();
    }

    private void OnInvited(bool invited)
    {
        if (_messageView != null)
            _messageView.Show(invited ? "Friends invited successful!" : "Friends inviting failed!");
    }

    private void OnShared(bool shared)
    {
        if (_messageView != null)
            _messageView.Show(shared ? "Sharing was successful!" : "Sharing was failed!");
    }

    private void OnLoggedIn(bool isLoggedIn)
    {
        _socialView.UpdateSocial(isLoggedIn);

        if (_messageView != null)
            _messageView.Show(isLoggedIn ? "You are logged in facebook!" : "You are logged out facebook!");
    }

    private void InitializeGui()
    {
        _messageView = _guiManager.GetView<MessageView>();
        _messageView.OkPressed += OnMessageOkPressed;

        _socialView = _guiManager.GetView<SocialView>();
        _socialView.LogInPressed += OnLoginButtonPressed;
        _socialView.SharePressed += OnShareButtonPressed;
        _socialView.InvitePressed += OnInviteButtonPressed;
        _socialView.LogOutPressed += OnLogoutPressed;
        _socialView.UpdateSocial(false);
    }

    private void OnMessageOkPressed()
    {
        _socialView.Show();
    }

    private void OnLogoutPressed()
    {
        _facebookController.Logout();
    }

    private void OnInviteButtonPressed()
    {
        _facebookController.InviteFriends();
    }

    private void OnShareButtonPressed()
    {
        _facebookController.Share();
    }

    private void OnLoginButtonPressed()
    {
        _facebookController.Login();   
    }
}
